package prototyping.View;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JComponent;

/* ************************************
 * A square with a position and a size.
 */
public class Square extends JComponent {

    private int width;
    private int height;
    private double xPosition;
    private double yPosition;
    private BufferedImage image;
    
    public Square(int width, int height){
        this.width = width;
        this.height = height;
        xPosition = 0;
        yPosition = 0;
        
        try {
            File f = new File("/Users/Connor/Documents/engi5895project/src/View/1 Panel Insert.png");
            image = ImageIO.read(f);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void setPosition(double x, double y){
        xPosition = x * width;
        yPosition = y * height;
        
    }
    
    public void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        
//        g2d.fill(new Rectangle2D.Double(xPosition, yPosition, width, height));
        
        AffineTransform trans = new AffineTransform(g2d.getTransform());
        trans.scale( (double) width/image.getWidth(), (double) height/image.getHeight());
        g2d.drawImage(image,
                new AffineTransformOp(trans, AffineTransformOp.TYPE_NEAREST_NEIGHBOR),
                (int) xPosition, (int) yPosition);
    }
}
