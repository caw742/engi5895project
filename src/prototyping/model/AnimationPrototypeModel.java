package prototyping.model;

import java.time.Duration;
import java.time.Instant;

import prototyping.View.ViewPrototype;
import prototyping.comms.AnimationClientPrototype;

/* ***********************************************************************************
 * Model for a square in a grid. Creates a client which updates the square's position.
 * When the position is updated it refreshes the view.
 */
public class AnimationPrototypeModel {

    private static int MODEL_WIDTH = 10;
    private static int MODEL_HEIGHT = 10;

    private double squareX;
    private double squareY;
    private double xSpeed;
    private double ySpeed;
    private Instant previous;
    private AnimationClientPrototype client;
    
    public AnimationPrototypeModel(ViewPrototype view){
        squareX = 0;
        squareY = 0;
        xSpeed = 0;
        ySpeed = 0;
        previous = Instant.now();
        client = new AnimationClientPrototype(this);
        client.initComms();
        Thread viewUpdateThread = new Thread(new Runnable(){
            public void run(){
                while(true){
                    updatePositions();
                    view.refresh();
                }
            }
        });
        viewUpdateThread.start();
    }

    public void update(String direction){
        if (direction.equals("up") && squareY > 0){
            ySpeed -= 1;
        } else if (direction.equals("down") && squareY < (MODEL_HEIGHT-1)){
            ySpeed += 1;
        } else if (direction.equals("left") && squareX > 0){
            xSpeed -= 1;
        } else if (direction.equals("right") && squareX < (MODEL_WIDTH-1)){
            xSpeed += 1;
        }
    }
    
    public int getWidth(){
        return MODEL_WIDTH;
    }
    
    public int getHeight(){
        return MODEL_HEIGHT;
    }
    
    public double getX(){
        updatePositions();
        return squareX;
    }
    
    public double getY(){
        updatePositions();
        return squareY;
    }
    
    private void updatePositions(){
      Instant now = Instant.now();
      Duration timeElapsed = Duration.between(previous, now);
      previous = now;

      squareX += (timeElapsed.getNano()/1000000000d) * xSpeed;
      squareY += (timeElapsed.getNano()/1000000000d) * ySpeed;
      
      if (squareX < 0){
          squareX = 0;
      } else if (squareX > (MODEL_WIDTH-1)){
          squareX = MODEL_WIDTH-1;
      } else if (squareY < 0){
          squareY = 0;
      } else if (squareY > (MODEL_HEIGHT-1)){
          squareY = MODEL_HEIGHT-1;
      }
      
    }
}
